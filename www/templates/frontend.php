<!doctype>
<html>
<head>
	<title><?php echo App::getInstance()->getParam('title'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?php echo Config::get('global')['css_dir']; ?>main.css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
</head>
<body>
    <h1 class="hide">My Website</h1>
    <?php echo $__content; ?>
</body>
</html>