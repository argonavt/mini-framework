<?php

/**
 * Created by argonavt.
 * Date: 20.01.17
 * Time: 14:48
 */

/**
 * Class Config keeps project's config data
 */
class Config
{
    /**
	 * @var array $__data
	 */
	private static $__data = [
	    'global' => [
	        'debug' => False,
	        'site_template' => 'frontend',
		    'admin_template' => 'backend',
		    'css_dir' => '/static/css/',
		    'js_dir' => '/static/js/',
		    'SendGridAPI_key' => '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',
	    ],

	    'routes' => [
	        'admin' => 'user/BackendController/actionIndex',
	        'admin/login' => 'user/BackendController/actionLogin',
	        'admin/logout' => 'user/BackendController/actionLogout',
		    'admin/users' => 'user/BackendController/actionUserList',
		    'admin/user/delete/(\d+)' => 'user/BackendController/actionDelete/id:$1',
		    'admin/user/add' => 'user/BackendController/actionAdd',
		    'admin/user/change' => 'user/BackendController/actionChange',
	        '404' => 'system/ErrorController/action404',
	    ],

//	    'db' => [
//	        'host' => 'db:3306',
//	        'user' => 'user',
//	        'pass' => 'pass',
//	        'database' => 'database',
//	    ],
	];

    /**
     * Return config block
     *
     * @param string $name
     * @return array
     */
    public static function get(string $name): array {
        return self::$__data[$name];
    }

	/**
	 * Get current time DateTime object
	 *
	 * @param string $time
	 * @return DateTime
	 */
    public static function getDateTime($time = 'now'){
        return new DateTime($time, new DateTimeZone('America/Toronto'));
    }
}
