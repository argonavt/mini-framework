-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `link` varchar(60) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `admin_menu_admin_menu_id_fk` (`parent`),
  CONSTRAINT `admin_menu_admin_menu_id_fk` FOREIGN KEY (`parent`) REFERENCES `admin_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `admin_menu` (`id`, `name`, `link`, `parent`, `sort`, `active`) VALUES
(1,	'Main',	'/admin',	NULL,	0,	1),
(2,	'Users',	'/admin/users',	NULL,	0,	1),
(3,	'Logout',	'/admin/logout',	NULL,	99,	1),
(4,	'Change',	'/admin/user/change',	NULL,	0,	1);

DROP TABLE IF EXISTS `admin_menu_roles`;
CREATE TABLE `admin_menu_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_menu_roles_admin_menu_id_fk` (`menu_id`),
  KEY `admin_menu_roles_roles_id_fk` (`role_id`),
  CONSTRAINT `admin_menu_roles_admin_menu_id_fk` FOREIGN KEY (`menu_id`) REFERENCES `admin_menu` (`id`),
  CONSTRAINT `admin_menu_roles_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `admin_menu_roles` (`id`, `menu_id`, `role_id`) VALUES
(1,	1,	1),
(2,	2,	1),
(3,	3,	1),
(4,	4,	1),
(5,	1,	2),
(6,	3,	2),
(7,	4,	2);

DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(60) DEFAULT NULL,
  `date` datetime NOT NULL,
  `ip` varchar(30) NOT NULL,
  `success` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `login_log` ( `login`, `date`, `ip`, `success`) VALUES
('argonavt',	'2017-02-28 20:54:56', '172.27.0.1',	1);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_uindex` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `roles` (`id`, `name`) VALUES
(1,	'Admin'),
(2,	'User');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `mail` varchar(90) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `creation_datetime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_login_uindex` (`login`),
  UNIQUE KEY `users_mail_uindex` (`mail`),
  KEY `users_roles_id_fk` (`role`),
  CONSTRAINT `users_roles_id_fk` FOREIGN KEY (`role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `login`, `mail`, `pass`, `role`, `creation_datetime`) VALUES
(3,	'argonavt',	'zaloznyi.sasha@gmail.com',	'$2y$10$e/li07riHad603bUL2AWae5li7V6C1v7A2VTjM8F04aL8og3GIFgu',	1,	'2017-02-28 18:41:18');

-- 2017-02-28 23:42:21
