<?php

/**
 * Created by argonavt.
 * Date: 20.01.17
 * Time: 22:29
 */

trait Singleton {
    static private $instance = null;

    private function __construct() { /* ... @return Singleton */ }
    private function __clone() { /* ... @return Singleton */ }

    static public function getInstance() {
        return
            self::$instance===null
                ? self::$instance = new static()//new self()
                : self::$instance;
    }
}

/**
 * Class App
 * @method static App getInstance()
 */
class App
{
    use Singleton;

    /**
     * @var Router $__route
     */
    private $__route;

    /**
     * Custom params
     *
     * @var array $__params
     */
    private $__params = [];

    /**
     * Set router
     * @param Router $route
     */
    public function setRoute(Router $route){
        $this->__route = $route;
    }

	/**
	 * Execute Controller's action
	 *
	 * @param bool $return
	 */
    public function execute($return = False){
        $controller = $this->__route->getController();
        $action = $this->__route->getAction();

        if($return){
        	return $controller->$action();
        }

        $controller->$action();
    }

    /**
     * Set custom var
     *
     * @param $key
     * @param $value
     */
    public function setParam($key, $value){
        $this->__params[$key] = $value;
    }

    /**
     * Get custom var
     *
     * @param $key
     * @return $this->__params[$key]
     */
    public function getParam($key){
        return $this->__params[$key];
    }

    /**
     *  Load and return a model
     *
     * @param string $path
     * @return Model
     */
    public function loadModel(string $path): Model {
        list($module, $model) = explode('/', $path);
        $model = ucfirst($model).'Model';
        require $_SERVER['DOCUMENT_ROOT']."/modules/$module/Models/{$model}.php";
	    $model = '\\'.ucfirst($module).'\\'.$model;
        return new $model;
    }

    public function loadController(string $path, $return = True){
	    list($module, $controllerName, $actionName) = explode('/', $path);

	    $controllerName = ucfirst($controllerName).'Controller';
	    $actionName = 'action'.ucfirst($actionName);

	    $this->__route->setController($module, $controllerName, $actionName);

	    if($return){
		    return $this->execute(True);
	    }

	    $this->execute();
    }

    /**
     * Redirect to another url
     *
     * @param string $url
     * @param int $code
     */
    public function redirect(string $url, $code=302){
        header('Location: '.$url, True, $code);
        die;
    }

	/**
	 * Show error message
	 *
	 * @param string $msg
	 */
    public function showError(string $msg){
    	if(function_exists('xdebug_print_function_stack')){
		    xdebug_print_function_stack($msg);
		    die;
	    }

    	echo $msg . "<br><br>\n\n";
	    $e = new Exception();
	    print_r($e->getTraceAsString());
	    die;
    }

}