<?php

/**
 * Created by argonavt.
 * Date: 21.01.17
 * Time: 9:11
 */
abstract class BController extends Controller
{
	public function __construct(){
		parent::__construct();

		$this->template = Config::get('global')['admin_template'];
	}
}