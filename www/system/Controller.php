<?php

/**
 * Created by argonavt.
 * Date: 20.01.17
 * Time: 14:52
 */

/**
 * Class interface for Controllers
 */
abstract class Controller
{

	/**
	 * Keeps a name of template
	 *
	 * @var string $template
	 */
	protected $template;

    /**
     * Controller constructor.
     *
     * Set controller default data
     */
    public function __construct(){
        $class = new ReflectionClass($this);

        $this->viewDir = realpath(dirname($class->getFileName()).'/../Views');
        $this->modelDir = realpath(dirname($class->getFileName()).'/../Models');

        $this->template = Config::get('global')['site_template'];
    }

    /**
     * Render view with template
     *
     * @param string $view
     * @param array $data
     */
    protected function render(string $view, array $data){
	    $__content = $this->renderPartial($view, $data, True);
        require $_SERVER['DOCUMENT_ROOT'].'/templates/'.$this->template.'.php';
    }


	/**
	 * Render view
	 *
	 * @param string $view
	 * @param array $data
	 * @param bool $return
	 * @return string
	 */
    protected function renderPartial(string $view, array $data, $return = False){
	    if(file_exists($this->viewDir.'/'.$view.'.php')){
		    extract($data);
		    ob_start();
		    require $this->viewDir.'/'.$view.'.php';
		    $__content =  ob_get_clean();
	    }else{
		    if (Config::get('global')['debug']){
				App::getInstance()->showError('File '.$this->viewDir.'/'.$view.'.php was not found');
		    }else{
			    App::getInstance()->redirect('/404');
		    }
	    }

	    if($return){
	    	return $__content;
	    }

	    echo $__content;
    }

	protected function renderJson(array $data){
		header('Content-Type: application/json');
		echo json_encode($data);
	}
}