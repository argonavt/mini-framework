<?php
namespace Menu;
/**
 * Created by argonavt.
 * Date: 25.01.17
 * Time: 17:32
 */

class BackendController extends \BController
{

	/**
	 * Generate and return admin menu
	 *
	 * @return string view render
	 */
	public function actionMenu(){
		$model = \App::getInstance()->loadModel('menu/backendMenu');
		$data = [
			'menuList' => $model->getMenu(),
		];
		return $this->renderPartial('backend/menu', $data, True);
	}
}