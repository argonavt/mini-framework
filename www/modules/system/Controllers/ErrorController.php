<?php
namespace System;
/**
 * Created by argonavt.
 * Date: 20.01.17
 * Time: 16:53
 */

/**
 * Class ErrorController shows servers errors
 */
class ErrorController extends \Controller
{
    /**
     *  Show 404 page
     */
    public function action404(){
        $data = [
            'h1' => 'Page was not found'
        ];

        \App::getInstance()->setParam('title', '404');

        header("HTTP/1.0 404 Not Found");

        $this->render('error/404', $data);
    }
}