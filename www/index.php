<?php
/**
 * Created by argonavt.
 * Date: 20.01.17
 * Time: 14:46
 */

require 'config.php';

if (Config::get('global')['debug']){
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

require './vendor/autoload.php';

// autoload classes
spl_autoload_register(function ($className) {
    require "./system/$className.php";
});

// Routing
$route = new Router();
$route->setUrl($_SERVER['REQUEST_URI']);
$route->run();

session_start();

// Run controller
App::getInstance()->setRoute($route);
App::getInstance()->execute();
