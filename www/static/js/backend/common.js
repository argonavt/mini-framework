/**
 * Created by argonavt on 28.01.17.
 */
'use strict';

(function () {
    document.querySelector('#users form').addEventListener('submit', function (e) {
        e.preventDefault();

        var newUser = new NewUser(this);
        newUser.validate();
        newUser.send();
        newUser.showErrors();
    });
})();