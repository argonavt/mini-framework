const config = {
    'sass': './www/assets/sass/',
    'js': './www/assets/js/',
    'css': './www/static/css/',
    'js_compiled': './www/static/js/',
};

const gulp = require('gulp');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const autoprefixer = require("gulp-autoprefixer");

gulp.task('styles', () => {
    gulp.src(config.sass + '**/*.sass')
        .pipe(sass({outputStyle: "compact"}).on('error', sass.logError))
        .pipe(autoprefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"]))
        .pipe(gulp.dest(config.css));
});

gulp.task('js', () => {
    gulp.src(config.js  + '**/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(config.js_compiled));
});

//Watch task
gulp.task('default', ['styles', 'js'], () => {
    gulp.watch(config.sass + '**/*.sass', ['styles']);
    gulp.watch(config.js  + '**/*.js', ['js']);
});